package com.college.etut.model.response;

import com.college.etut.entity.enums.MajorType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TeacherResponse {
    private Long id;
    private String username;
    private String name;
    private String surname;
    private String phoneNumber;
    private MajorType majorType;
}
