package com.college.etut.model.response;

import com.college.etut.entity.enums.MajorType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class LessonResponse {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private MajorType majorType;
    private String teacherFullName;
    private String studentFullName;
}
