package com.college.etut.model.modelmapper;

import com.college.etut.entity.Lesson;
import com.college.etut.model.request.LessonRequest;
import com.college.etut.model.response.LessonResponse;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface LessonMapper {
    Lesson toEntity(LessonRequest lessonRequest);

    @Mapping(target = "teacherFullName", expression = "java(null == lesson.getTeacher() ? null : lesson.getTeacher().getName()+ \" \" + lesson.getTeacher().getSurname())")
    @Mapping(target = "studentFullName", expression = "java(null == lesson.getStudent() ? null : lesson.getStudent().getName()+ \" \" + lesson.getStudent().getSurname())")
    @Mapping(target = "majorType", source = "teacher.majorType")
    LessonResponse toResponse(Lesson lesson);

    List<LessonResponse> toResponse(List<Lesson> lessons);
}
