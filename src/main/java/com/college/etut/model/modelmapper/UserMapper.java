package com.college.etut.model.modelmapper;

import com.college.etut.entity.User;
import com.college.etut.model.request.StudentRequest;
import com.college.etut.model.request.TeacherRequest;
import com.college.etut.model.response.StudentResponse;
import com.college.etut.model.response.TeacherResponse;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User toEntity(StudentRequest studentRequest);

    User toEntity(TeacherRequest teacherRequest);

    StudentResponse toResponseStudent(User user);

    TeacherResponse toResponseTeacher(User user);

    List<StudentResponse> toResponseStudent(List<User> studentList);

    List<TeacherResponse> toResponseTeacher(List<User> teacherList);
}
