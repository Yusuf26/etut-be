package com.college.etut.model.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentUpdateRequest {

    @NotBlank
    private Long id;

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotBlank
    @Size(max = 255)
    private String surname;

    @NotBlank
    @Size(max = 50)
    @Pattern(regexp = "^\\d(\\d{3}[- .]?){2}(\\d{2}[- .]?){2}$")
    private String phoneNumber;

    @NotBlank
    @Size(max = 255)
    private String classCode;
}
