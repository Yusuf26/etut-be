package com.college.etut.model.request;

import com.college.etut.entity.enums.RoleType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StudentRequest {

    @NotBlank
    @Size(max = 100)
    private String username;

    @NotBlank
    @Size(max = 255)
    private String password;

    @JsonIgnore
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.STUDENT;

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotBlank
    @Size(max = 255)
    private String surname;

    @NotBlank
    @Size(max = 50)
    @Pattern(regexp = "^\\d(\\d{3}[- .]?){2}(\\d{2}[- .]?){2}$")
    private String phoneNumber;

    @NotBlank
    @Size(max = 255)
    private String classCode;
}
