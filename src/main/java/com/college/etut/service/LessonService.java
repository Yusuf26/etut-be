package com.college.etut.service;

import com.college.etut.entity.Lesson;
import com.college.etut.entity.User;
import com.college.etut.entity.enums.RoleType;
import com.college.etut.exceptions.ResourceNotFoundException;
import com.college.etut.model.modelmapper.LessonMapper;
import com.college.etut.model.request.LessonRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.LessonResponse;
import com.college.etut.repository.LessonRepository;
import com.college.etut.repository.UserRepository;
import com.college.etut.utils.ErrorMessages;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonService {

    private final LessonMapper lessonMapper;
    private final LessonRepository lessonRepository;
    private final UserRepository userRepository;

    @Transactional
    public GenericReturnValue<Long> save(LessonRequest lessonRequest) {
        Lesson lesson = lessonMapper.toEntity(lessonRequest);
        User teacher = userRepository.findByIdAndRoleTypeEquals(lessonRequest.getTeacherId(), RoleType.TEACHER)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND));
        lesson.setTeacher(teacher);
        return new GenericReturnValue<>(lessonRepository.save(lesson).getId());
    }

    @Transactional(readOnly = true)
    public List<LessonResponse> getTeacherLessonList(Long id) {
        if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.TEACHER))
            throw new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND);
        List<Lesson> lessonResponseList = lessonRepository.findAllByTeacher_Id(id);
        if (lessonResponseList.isEmpty())
            throw new ResourceNotFoundException(ErrorMessages.LESSON_NOT_FOUND);
        return lessonMapper.toResponse(lessonResponseList);
    }

    @Transactional(readOnly = true)
    public List<LessonResponse> getStudentLessonList(Long id) {
        if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.STUDENT))
            throw new ResourceNotFoundException(ErrorMessages.STUDENT_NOT_FOUND);
        List<Lesson> lessonResponseList = lessonRepository.findAllByStudent_Id(id);
        if (lessonResponseList.isEmpty())
            throw new ResourceNotFoundException(ErrorMessages.LESSON_NOT_FOUND);
        return lessonMapper.toResponse(lessonResponseList);
    }
}
