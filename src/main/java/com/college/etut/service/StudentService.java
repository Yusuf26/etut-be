package com.college.etut.service;

import com.college.etut.entity.User;
import com.college.etut.entity.enums.RoleType;
import com.college.etut.exceptions.ResourceAlreadyExists;
import com.college.etut.exceptions.ResourceNotFoundException;
import com.college.etut.model.modelmapper.UserMapper;
import com.college.etut.model.request.StudentRequest;
import com.college.etut.model.request.StudentUpdateRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.StudentResponse;
import com.college.etut.repository.UserRepository;
import com.college.etut.utils.ErrorMessages;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public GenericReturnValue<Long> saveStudent(StudentRequest studentRequest) {
        if (userRepository.existsByUsername(studentRequest.getUsername()))
            throw new ResourceAlreadyExists(ErrorMessages.USERNAME_ALREADY_EXIST);
        studentRequest.setPassword(passwordEncoder.encode(studentRequest.getPassword()));
        User student = userMapper.toEntity(studentRequest);
        return new GenericReturnValue<>(userRepository.save(student).getId());
    }

    @Transactional(readOnly = true)
    public StudentResponse getStudent(Long id) {
        User student = userRepository.findByIdAndRoleTypeEquals(id, RoleType.STUDENT)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessages.STUDENT_NOT_FOUND));
        return userMapper.toResponseStudent(student);
    }

    @Transactional(readOnly = true)
    public List<StudentResponse> getStudentList() {
        List<User> studentList = userRepository.findAllByRoleTypeEquals(RoleType.STUDENT);
        return userMapper.toResponseStudent(studentList);
    }

    @Transactional
    public GenericReturnValue<Long> deleteStudent(Long id) {
        if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.STUDENT))
            throw new ResourceNotFoundException(ErrorMessages.STUDENT_NOT_FOUND);
        userRepository.deleteById(id);
        return new GenericReturnValue<>(id);
    }

    @Transactional
    public GenericReturnValue<List<Long>> deleteStudentList(List<Long> ids) {
        ids.forEach(id -> {
            if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.STUDENT))
                throw new ResourceNotFoundException(ErrorMessages.STUDENT_NOT_FOUND);
        });
        userRepository.deleteInBatch(userRepository.findAllById(ids));
        return new GenericReturnValue<>(ids);
    }

    @Transactional
    public GenericReturnValue<Integer> updateStudent(StudentUpdateRequest studentUpdateRequest) {
        if (!userRepository.existsById(studentUpdateRequest.getId()) ||
                !userRepository.existsByIdAndRoleTypeEquals(studentUpdateRequest.getId(), RoleType.STUDENT))
            throw new ResourceNotFoundException(ErrorMessages.STUDENT_NOT_FOUND);
        return new GenericReturnValue<>(userRepository.updateStudent(studentUpdateRequest));
    }
}
