package com.college.etut.service;

import com.college.etut.entity.UserToken;
import com.college.etut.repository.UserTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserTokenService {

    private final UserTokenRepository tokenRepository;

    public void setToken(UserToken userToken) {
        tokenRepository.save(userToken);
    }

    public void deleteToken(String username) {
        tokenRepository.deleteById(username);
    }

    public boolean existUserToken(String username) {
        return tokenRepository.existsById(username);
    }
}
