package com.college.etut.service;

import com.college.etut.entity.User;
import com.college.etut.entity.enums.RoleType;
import com.college.etut.exceptions.ResourceAlreadyExists;
import com.college.etut.exceptions.ResourceNotFoundException;
import com.college.etut.model.modelmapper.UserMapper;
import com.college.etut.model.request.TeacherRequest;
import com.college.etut.model.request.TeacherUpdateRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.TeacherResponse;
import com.college.etut.repository.UserRepository;
import com.college.etut.utils.ErrorMessages;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TeacherService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Transactional
    public GenericReturnValue<Long> saveTeacher(TeacherRequest teacherRequest) {
        if (userRepository.existsByUsername(teacherRequest.getUsername()))
            throw new ResourceAlreadyExists(ErrorMessages.USERNAME_ALREADY_EXIST);
        teacherRequest.setPassword(passwordEncoder.encode(teacherRequest.getPassword()));
        User student = userMapper.toEntity(teacherRequest);
        return new GenericReturnValue<>(userRepository.save(student).getId());
    }

    @Transactional(readOnly = true)
    public TeacherResponse getTeacher(Long id) {
        User teacher = userRepository.findByIdAndRoleTypeEquals(id, RoleType.TEACHER)
                .orElseThrow(() -> new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND));
        return userMapper.toResponseTeacher(teacher);
    }

    @Transactional(readOnly = true)
    public List<TeacherResponse> getTeacherList() {
        List<User> teacherList = userRepository.findAllByRoleTypeEquals(RoleType.TEACHER);
        return userMapper.toResponseTeacher(teacherList);
    }

    @Transactional
    public GenericReturnValue<Long> deleteTeacher(Long id) {
        if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.TEACHER))
            throw new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND);
        userRepository.deleteById(id);
        return new GenericReturnValue<>(id);
    }

    @Transactional
    public GenericReturnValue<List<Long>> deleteTeacherList(List<Long> ids) {
        ids.forEach(id -> {
            if (!userRepository.existsByIdAndRoleTypeEquals(id, RoleType.TEACHER))
                throw new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND);
        });
        userRepository.deleteInBatch(userRepository.findAllById(ids));
        return new GenericReturnValue<>(ids);
    }

    @Transactional
    public GenericReturnValue<Integer> updateTeacher(TeacherUpdateRequest teacherUpdateRequest) {
        if (!userRepository.existsById(teacherUpdateRequest.getId()) ||
                !userRepository.existsByIdAndRoleTypeEquals(teacherUpdateRequest.getId(), RoleType.TEACHER))
            throw new ResourceNotFoundException(ErrorMessages.TEACHER_NOT_FOUND);
        return new GenericReturnValue<>(userRepository.updateTeacher(teacherUpdateRequest));
    }
}
