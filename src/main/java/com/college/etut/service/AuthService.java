package com.college.etut.service;

import com.college.etut.model.response.GenericReturnValue;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserTokenService userTokenService;

    public GenericReturnValue<Boolean> logout() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        userTokenService.deleteToken(authentication.getName());
        return new GenericReturnValue<>(true);
    }
}
