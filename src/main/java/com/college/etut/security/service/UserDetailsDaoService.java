package com.college.etut.security.service;

import com.college.etut.entity.User;
import com.college.etut.security.UserDetailsDao;
import com.college.etut.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserDetailsDaoService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUserName(username);
        return UserDetailsDao.builder().username(user.getUsername()).password(user.getPassword()).id(user.getId())
                .authorities(user.getRoleType().getPermissions()).isAccountNonExpired(true).isAccountNonLocked(true)
                .isCredentialsNonExpired(true).isEnabled(true).build();
    }
}
