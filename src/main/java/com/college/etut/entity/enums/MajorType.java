package com.college.etut.entity.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

public enum MajorType {
    MATEMATIK, GEOMETRI, FIZIK, KIMYA, BIYOLOJI, TURKCE, TARIH, COGRAFYA, FELSEFE, INGILIZCE;

    @JsonValue
    public String getMajorType() {
        return this.name();
    }

    @JsonCreator
    public MajorType getType(String majorType) {
        return Arrays.stream(values()).filter(x -> x.name().equals(majorType)).findFirst().get();
    }

}
