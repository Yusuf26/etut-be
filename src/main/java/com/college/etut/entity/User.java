package com.college.etut.entity;

import com.college.etut.entity.enums.MajorType;
import com.college.etut.entity.enums.RoleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_id")
    @SequenceGenerator(name = "user_id", sequenceName = "user_id_sequence", allocationSize = 100)
    private Long id;

    @Column(unique = true)
    @NotBlank
    @Size(max = 100)
    private String username;

    @NotBlank
    @Size(max = 255)
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @NotBlank
    @Size(max = 255)
    private String name;

    @NotBlank
    @Size(max = 255)
    private String surname;

    @NotBlank
    @Size(max = 50)
    @Pattern(regexp = "^\\d(\\d{3}[- .]?){2}(\\d{2}[- .]?){2}$")
    private String phoneNumber;

    @Size(max = 255)
    private String classCode;

    @Enumerated(EnumType.STRING)
    private MajorType majorType;

    @OneToMany(mappedBy = "teacher", fetch = FetchType.LAZY)
    private Set<Lesson> teacherLessons;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    private Set<Lesson> studentLessons;

}
