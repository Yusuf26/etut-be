package com.college.etut.controller;

import com.college.etut.model.request.StudentUpdateRequest;
import com.college.etut.model.request.TeacherRequest;
import com.college.etut.model.request.TeacherUpdateRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.TeacherResponse;
import com.college.etut.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @PostMapping
    public ResponseEntity<GenericReturnValue<Long>> saveTeacher(@RequestBody TeacherRequest teacherRequest) {
        return ResponseEntity.ok(teacherService.saveTeacher(teacherRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<TeacherResponse> getTeacher(@PathVariable(name = "id") Long id) {
        return ResponseEntity.ok(teacherService.getTeacher(id));
    }

    @GetMapping
    public ResponseEntity<List<TeacherResponse>> getTeacherList() {
        return ResponseEntity.ok(teacherService.getTeacherList());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GenericReturnValue<Long>> deleteTeacher(@PathVariable Long id) {
        return ResponseEntity.ok(teacherService.deleteTeacher(id));
    }

    @DeleteMapping("/deleteAll/{ids}")
    public ResponseEntity<GenericReturnValue<List<Long>>> deleteTeacherList(@PathVariable List<Long> ids) {
        return ResponseEntity.ok(teacherService.deleteTeacherList(ids));
    }

    @PutMapping
    public ResponseEntity<GenericReturnValue<Integer>> updateTeacher(@RequestBody TeacherUpdateRequest teacherUpdateRequest) {
        return ResponseEntity.ok(teacherService.updateTeacher(teacherUpdateRequest));
    }
}
