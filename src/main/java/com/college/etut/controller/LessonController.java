package com.college.etut.controller;

import com.college.etut.model.request.LessonRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.LessonResponse;
import com.college.etut.service.LessonService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lesson")
public class LessonController {

    private final LessonService lessonService;

    @PostMapping
    public ResponseEntity<GenericReturnValue<Long>> save(@RequestBody LessonRequest lessonRequest) {
        return ResponseEntity.ok(lessonService.save(lessonRequest));
    }

    @GetMapping("/teacher/{id}")
    public ResponseEntity<List<LessonResponse>> getTeacherLessons(@PathVariable Long id) {
        return ResponseEntity.ok(lessonService.getTeacherLessonList(id));
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<List<LessonResponse>> getStudentLessons(@PathVariable Long id) {
        return ResponseEntity.ok(lessonService.getStudentLessonList(id));
    }
}
