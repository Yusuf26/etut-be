package com.college.etut.controller;

import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    /**
     * @PostMapping("/login") API already implemented by Spring Security
     */

    @PostMapping("/logout")
    public ResponseEntity<GenericReturnValue<Boolean>> logout() {
        return ResponseEntity.ok(authService.logout());
    }
}