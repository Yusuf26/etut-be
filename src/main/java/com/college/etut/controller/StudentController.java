package com.college.etut.controller;

import com.college.etut.model.request.StudentRequest;
import com.college.etut.model.request.StudentUpdateRequest;
import com.college.etut.model.response.GenericReturnValue;
import com.college.etut.model.response.StudentResponse;
import com.college.etut.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<GenericReturnValue<Long>> saveStudent(@RequestBody StudentRequest studentRequest) {
        return ResponseEntity.ok(studentService.saveStudent(studentRequest));
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentResponse> getStudent(@PathVariable Long id) {
        return ResponseEntity.ok(studentService.getStudent(id));
    }

    @GetMapping
    public ResponseEntity<List<StudentResponse>> getStudentList() {
        return ResponseEntity.ok(studentService.getStudentList());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<GenericReturnValue<Long>> deleteStudent(@PathVariable Long id) {
        return ResponseEntity.ok(studentService.deleteStudent(id));
    }

    @DeleteMapping("/deleteAll/{ids}")
    public ResponseEntity<GenericReturnValue<List<Long>>> deleteStudentList(@PathVariable List<Long> ids) {
        return ResponseEntity.ok(studentService.deleteStudentList(ids));
    }

    @PutMapping
    public ResponseEntity<GenericReturnValue<Integer>> updateStudent(@RequestBody StudentUpdateRequest studentUpdateRequest) {
        return ResponseEntity.ok(studentService.updateStudent(studentUpdateRequest));
    }
}
