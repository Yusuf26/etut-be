package com.college.etut.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ErrorMessages {
    public static final String USERNAME_ALREADY_EXIST = "This username is already used";
    public static final String USERNAME_NOT_FOUND = "This username is not found";
    public static final String AUTHENTICATION_ERROR = "Authentication information is wrong";
    public static final String USER_NOT_FOUND = "Cannot find user";
    public static final String STUDENT_NOT_FOUND = "Student not found";
    public static final String TEACHER_NOT_FOUND = "Teacher not found";
    public static final String LESSON_NOT_FOUND = "Lesson not found";
}
