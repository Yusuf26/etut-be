package com.college.etut.repository;

import com.college.etut.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {

    List<Lesson> findAllByTeacher_Id(Long id);

    List<Lesson> findAllByStudent_Id(Long id);
}

