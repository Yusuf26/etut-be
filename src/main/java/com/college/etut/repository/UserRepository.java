package com.college.etut.repository;

import com.college.etut.entity.User;
import com.college.etut.entity.enums.RoleType;
import com.college.etut.model.request.StudentUpdateRequest;
import com.college.etut.model.request.TeacherUpdateRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    boolean existsByUsername(String username);

    Optional<User> findByIdAndRoleTypeEquals(Long id, RoleType roleType);

    List<User> findAllByRoleTypeEquals(RoleType roleType);

    boolean existsByIdAndRoleTypeEquals(Long id, RoleType roleType);

    @Modifying
    @Query("UPDATE User u SET u.name = :#{#studentUpdateRequest.name}, u.surname = :#{#studentUpdateRequest.surname}, " +
            "u.phoneNumber = :#{#studentUpdateRequest.phoneNumber}, u.classCode = :#{#studentUpdateRequest.classCode} WHERE u.id = :#{#studentUpdateRequest.id}")
    int updateStudent(StudentUpdateRequest studentUpdateRequest);

    @Modifying
    @Query("UPDATE User u SET u.name = :#{#teacherUpdateRequest.name}, u.surname = :#{#teacherUpdateRequest.surname}, " +
            "u.phoneNumber = :#{#teacherUpdateRequest.phoneNumber}, u.majorType = :#{#teacherUpdateRequest.majorType} WHERE u.id = :#{#teacherUpdateRequest.id}")
    int updateTeacher(TeacherUpdateRequest teacherUpdateRequest);
}