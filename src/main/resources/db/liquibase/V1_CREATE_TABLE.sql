-- CREATE t_user table
CREATE sequence if not exists user_id_sequence increment 100;
CREATE TABLE if not exists t_user
(
    id         int8         NOT NULL default nextval('user_id_sequence'),
    username   varchar(100) NOT NULL,
    "password" varchar(255) NOT NULL,
    role_type  varchar(50)  NOT NULL,
    "name"       varchar(255) NOT NULL,
    surname      varchar(255) NOT NULL,
    phone_number varchar(50)  NOT NULL,
    class_code   varchar(255),
    major_type   varchar(50),
    CONSTRAINT t_user_pkey PRIMARY KEY (id)
);


-- CREATE t_lesson table
CREATE sequence if not exists lesson_id_sequence increment 100;
CREATE TABLE if not exists t_lesson
(
    id          int8 NOT NULL default nextval('lesson_id_sequence'),
    end_time    timestamp NOT NULL,
    start_time  timestamp NOT NULL,
    student_id  int8 NULL,
    teacher_id  int8 NULL,
    CONSTRAINT t_lesson_pkey PRIMARY KEY (id)
);

ALTER TABLE t_lesson
    ADD CONSTRAINT fkct4mdhj06sw6bh0as1d63fqw2 FOREIGN KEY (teacher_id) REFERENCES t_user (id);
ALTER TABLE t_lesson
    ADD CONSTRAINT fkt5rfm6rbkt0bqxm7q41ha7fhk FOREIGN KEY (student_id) REFERENCES t_user (id);